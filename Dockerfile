FROM alpine:edge
COPY . /root/capturebate

RUN apk --update --no-cache add python py-pip libxml2 libxslt libffi \
 && apk --update --no-cache add --virtual build-dep git python-dev gcc musl-dev linux-headers libxml2-dev libxslt-dev libffi-dev openssl-dev \
# && git clone https://gitlab.com/double_you/capturebate /root/capturebate \
 && cd /root/capturebate && pip install --no-cache-dir -r requirements.txt \
 && apk del build-dep --no-cache \
 && rm -Rf /tmp/*

CMD cd /root/capturebate && python main.py

